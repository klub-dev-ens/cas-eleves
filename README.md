# CAS élèves ENS

Serveur d'authentification central basé sur
[django-cas-server](https://github.com/nitmir/django-cas-server), en Django.

## Installation

```bash
# Récupérer le code
git clone [ce dépôt]
cd cas_eleves

# Créer un virtualenv et installer les dépendances
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
pip install gunicorn  # Serveur WSGI
pip install psycopg2  # Si base de données postgresql

# Créer la configuration
cp cas_eleves/settings.X.py settings.py  # où X est dev ou prod
$EDITOR cas_eleves/settings.py  # et configurer les FIXME

# Préparer la base de données
./manage.py migrate

# Préparer les fichiers statiques
./manage.py collectstatic

# Préparer les traductions
./manage.py compilemessages
```

À partir de là, on peut configurer un serveur WSGI (par exemple gunicorn) et un
reverse-proxy (par exemple nginx). Le reverse-proxy doit servir directement
`/static` depuis `$(pwd)/public/static`, par exemple (nginx) avec

```
location /static {
  root /path/to/this/directory/public;
  access_log off;
  add_header Cache-Control "public";
  expires 7d;
}
```

Le site devrait alors fonctionner.

Il faut également configurer un cron, timer systemd ou autre mécanisme pour
exécuter régulièrement `manage.py clearsessions`, `manage.py cas_clean_tickets`
et `manage.py cas_clean_sessions`.

## Configuration

Il faudra également configurer un superutilisateur Django :

```
./manage.py createsuperuser
```

puis se connecter sur `/admin` pour ajouter au moins un "service pattern". Sans
ça, personne ne pourra utiliser le CAS.
